@echo off 
echo "run in prompt:"
IF EXIST MTImemo.tex.pdf del MTImemo.tex.pdf
IF EXIST out.txt del out.txt
pdflatex MTImemo.tex >> out.txt
makeglossaries MTImemo.tex >> out.txt
bibtex MTImemo.tex >> out.txt
pdflatex MTImemo.tex >> out.txt
pdflatex MTImemo.tex >> out.txt


del *.log *.bak *.aux *.bbl *.blg *.idx *.toc *.out *.acn *.glo *.ssg *.syg *.ist *.glg *.gls *.slg *.ssi *.ssl *.syi *.acr *.alg *.xdy