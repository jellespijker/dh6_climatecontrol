\usetikzlibrary{positioning}
\tikzset{
pics/hamburger/.style args={#1/#2/#3/#4/#5}{
	code =
	{
		\node (functext)[inner sep=1, opacity=0] at (0,0.7) {#3};
		\node (id)[inner sep=1, opacity=0] at (0,0) {#5};
		\node (actuatortext)[inner sep=1, opacity=0] at (0,-0.7) {#4};
		\node (westtop) at (functext.west |- id.north) {};
		\node (easttop) at (functext.east |- id.north) {};
		\node (westbottom) at (functext.west |- id.south) {};
		\node (eastbottom) at (functext.east |- id.south) {};
		\draw [rounded corners=1.5mm, fill=#1!20] (westtop.west) ++ (0,0.4) --  ([yshift=3]westtop.west) -- ([yshift=3]easttop.east) -- (easttop.east |- functext.north) .. controls([yshift=15]functext.north) .. (westtop.west |- functext.north)  -- cycle;
		\draw [rounded corners, fill=#2!20] (westbottom.west) ++ (0,-0.4) --  ([yshift=-3]westbottom.west) -- ([yshift=-3]eastbottom.east) -- (eastbottom.east |- actuatortext.south) .. controls([yshift=-15]actuatortext.south) .. (westbottom.west |- actuatortext.south)  -- cycle;
		\draw [rounded corners, fill=gray!20] (westtop.west) -- (easttop.east) -- (eastbottom.east) -- (westbottom.west) -- cycle;
		\node (toptext)[inner sep=1] at (0,0.6) {#3};
		\node (idtext)[inner sep=1] at (0,0) {#5};
		\node (bottomtext)[inner sep=1] at (0,-0.7) {#4};
		\coordinate (-north) at ([yshift=12]functext.north);
		\coordinate (-south) at ([yshift=-12]actuatortext.south);
		\coordinate (-west) at (id.west);
		\coordinate (-east) at (id.east);
		}
	}
}

\begin{tikzpicture}
\pic (main) {hamburger=RoyalRed/RoyalGrey/Create comfort/Climate control/F1};
\coordinate (layerF2) at  ([yshift=-130]main-north);

\pic (F22) at (layerF2) {hamburger=yellow/VioletRed/Control Temperature/Heat/F2.2};
\pic (F21) at ([xshift=-290]F22-west) {hamburger=yellow/blue/Control humid air/Configuration/F2.1};
\pic (F23) at ([xshift=290]F22-east) {hamburger=yellow/green/Transport air/Air system/F2.3};
\coordinate (belowmain) at ($(main-south)!0.5!(F22-north)$);

\coordinate (layerF3) at ([yshift=-130]F21-north);
\coordinate (layerF4) at ([yshift=-130]F22-north);
\coordinate (layerF5) at ([yshift=-130]F23-north);

\pic (F42) at ([xshift=-30]layerF4) {hamburger=VioletRed/SandyBrown/Remove heat/System/F4.2};
\pic (F41) at ([xshift=-70]F42-west) {hamburger=VioletRed/SandyBrown/Create heat/System/F4.1};
\pic (F43) at ([xshift=70]F42-east) {hamburger=VioletRed/SandyBrown/Transfer heat/System/F4.3};
\pic (F44) at ([xshift=70]F43-east) {hamburger=VioletRed/SandyBrown/Transport heat/System/F4.4};

\pic (F32) at (layerF3) {hamburger=blue/SandyBrown/Add water/System/F3.2};
\pic (F31) at ([xshift=-70]F32-west) {hamburger=blue/SandyBrown/Remove water/System/F3.1};
\pic (F33) at ([xshift=70]F32-east) {hamburger=blue/SandyBrown/Seperate particles/System/F3.3};

\pic (F52) at (layerF5) {hamburger=green/SandyBrown/Contain air/System/F5.2};
\pic (F51) at ([xshift=-70]F52-west) {hamburger=green/SandyBrown/Diffuse air/System/F5.1};
\pic (F53) at ([xshift=70]F52-east) {hamburger=green/SandyBrown/Move air/System/F5.1};

\coordinate (belowF21) at ($(F21-south)!0.5!(F32-north)$);
\coordinate (belowF22) at ($(F22-south)!0.5!(F22-south |- F42-north)$);
\coordinate (belowF23) at ($(F23-south)!0.5!(layerF5 |- F51-north)$);

\draw (main-south) -- (belowmain);
\draw[-latex] (belowmain) -| (F21-north);
\draw[-latex] (belowmain) -| (F22-north);
\draw[-latex] (belowmain) -| (F23-north);

\draw (F21-south) -- (belowF21);
\draw[-latex] (belowF21) -| (F31-north);
\draw[-latex] (belowF21) -| (F32-north);
\draw[-latex] (belowF21) -| (F33-north);

\draw (F22-south) -- (belowF22);
\draw[-latex] (belowF22) -| (F41-north);
\draw[-latex] (belowF22) -| (F42-north);
\draw[-latex] (belowF22) -| (F43-north);
\draw[-latex] (belowF22) -| (F44-north);

\draw (F23-south) -- (belowF23);
\draw[-latex] (belowF23) -| (F51-north);
\draw[-latex] (belowF23) -| (F52-north);
\draw[-latex] (belowF23) -| (F53-north);

\end{tikzpicture}
