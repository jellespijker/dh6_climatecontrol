import matplotlib
import matplotlib.pyplot as plt

matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.unicode'] = True
from matplotlib.sankey import Sankey

def sum_tuple(a, b):
    return tuple([sum(x) for x in zip(a, b)])


fig = plt.figure(figsize=(5.73, 4.07), dpi=100)
ax = fig.add_subplot(1, 1, 1, xticks=[], yticks=[])
sankey = Sankey(ax=ax, unit=None)
sankey.add(flows=[1., 0.05, 0.25, -1.3],
           labels=['$ Q_{rc} $', r'$ Q_{ext} $', r'$ P_{eff} $', r'$ Q_{con} $'],
           pathlengths=[1., 0.15, 0.15, 0.15],
           label='Heat energy',
           trunklength=2.,
           orientations=[0, -1, -1, 0],
           facecolor='#ccffff')

diagrams = sankey.finish()
for d in diagrams:
    for t in d.texts:
        t.set_fontsize(16)
        t.set_weight('black')

plt.legend(loc='lower right')
#fig.show()
plt.savefig(r'./report/sankeyrefrigeratorsystem.pgf', dpi=600, bbox_inches='tight',
            orientation='portrait', papertype='a5')
print('finished!')
