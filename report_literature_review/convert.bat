@echo off 
echo "run in prompt:"
IF EXIST MTIreport.pdf del MTIreport.pdf
IF EXIST out.txt del out.txt
pdflatex MTIreport.tex >> out.txt
makeglossaries MTIreport >> out.txt
bibtex MTIreport >> out.txt
pdflatex MTIreport.tex >> out.txt
pdflatex MTIreport.tex >> out.txt


del *.log *.bak *.aux *.bbl *.blg *.idx *.toc *.out *.acn *.glo *.ssg *.syg *.ist *.glg *.gls *.slg *.ssi *.ssl *.syi *.acr *.alg *.xdy