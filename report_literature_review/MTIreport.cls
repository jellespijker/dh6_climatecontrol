\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{MTIreport}[2017/09/19 v1.1 MTI report class]
\LoadClass[paper=a4]{report}
\RequirePackage[dutch,english]{babel}
\RequirePackage[left=2.25cm,right=1.5cm,top=1cm,bottom=6.2cm]{geometry}

\usepackage{lipsum}
\usepackage{lastpage}
\usepackage{afterpage}
\usepackage{titlesec}
\usepackage{cancel}
\usepackage{xparse}
\usepackage{blindtext}
\usepackage{amsmath}
\usepackage{bm}
\usepackage[hidelinks]{hyperref}
\usepackage[svgnames]{xcolor}
\usepackage{float}
\usepackage{multirow}
\usepackage{calc}
\usepackage{booktabs}
\usepackage{eso-pic}
\usepackage{incgraph}
\usepackage{url}
\usepackage{csquotes}
\usepackage{etoolbox}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{tabu}
\usepackage{chemformula}
\usepackage{physics}
\usepackage{subcaption}
\usepackage[title,titletoc]{appendix}

% ===FONT=======================================================================
\usepackage[default]{lato}
\usepackage[T1]{fontenc}
\definecolor{mti_gray}{HTML}{A9A9A9}

% ===HEADINGS===================================================================
\titleformat{\chapter}{\normalfont\huge}{\thechapter.}{20pt}{\huge\it}
\titlespacing*{\chapter}{0cm}{0cm}{1cm}

% ===MTI HEADER=================================================================
\def\MTIname{IHC MTI B.V.}
\def\MTIBox{P.O. Box 2, 2600 MB  Delft}
\def\MTIadress{Delftechpark 13, 2628 XJ  Delft}
\def\MTItel{T +31 88 015 2535}
\def\MTImail{info@ihcmti.com}
\def\MTIsite{www.ihcmti.com}

\newcommand{\reportdate}[1]{\def\@reportdate{#1}}
\newcommand{\reportnumber}[1]{\def\@reportnumber{#1}}
\newcommand{\reportversion}[1]{\def\@reportversion{#1}}
\newcommand{\reportstatus}[1]{\def\@reportstatus{#1}}
\newcommand{\reportauthor}[1]{\def\@reportauthor{#1}}
\newcommand{\reportreviewFirst}[1]{\def\@reportreviewFirst{#1}}
\newcommand{\reportreviewSecond}[1]{\def\@reportreviewSecond{#1}}
\newcommand{\reportreviewThird}[1]{\def\@reportreviewThird{#1}}
\newcommand{\reportapprove}[1]{\def\@reportapprove{#1}}
\newcommand{\reporttitle}[1]{\def\@reporttitle{#1}}
\newcommand{\reportsubtitle}[1]{\def\@reportsubtitle{#1}}
\newcommand{\reportclient}[1]{\def\@reportclient{#1}}
\newcommand{\reportRefNum}[1]{\def\@reportRefNum{#1}}
\newcommand{\reportclassification}[1]{\def\@reportclassification{#1}}
\newcommand{\ordernum}[1]{\def\@ordernum{#1}}\RequirePackage{fancyhdr}

\RequirePackage{filecontents}
\setlength{\headheight}{3.5cm}
\pagestyle{fancy}
\fancypagestyle{plain}{}
\fancyhf{}

\fancyhead[L]{
	\begin{tabular}{p{3 cm} p{3 cm} p{3 cm} p{3 cm} p{3 cm}}
		\includegraphics[height=1.5cm]{resources/MTIlogo.png}\\
		\footnotesize{Date}&\footnotesize{Report number} & \footnotesize{Version} & \footnotesize{Status} & \footnotesize{Page}\\
		\footnotesize{\@reportdate} & \footnotesize{\@reportnumber}  & \footnotesize{\@reportversion} & \footnotesize{\@reportstatus} &\footnotesize{\thepage\ of \pageref{LastPage}}
\end{tabular}}

\fancyfoot[C,CO]{\footnotesize{This document remains the property of \MTIname. All rights reserved. This document or any part thereof may not be made public or disclosed, copied or otherwise reproduced or used in any form or by any means, without prior permission in writing from \MTIname.}}

\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

% ===CONTROLPAGE================================================================
\newcommand{\makequalitycontrol}{
	\noindent \textbf{{\large  Quality control}} \\[0.5cm]
	\noindent This report has been reviewed and approved in accordance with the policies of IHC MTI B.V. \\

	\begin{table}[H]
		\begin{tabular}{p{3cm}|p{4cm}|p{3cm}|p{5cm}|}
			\cline{2-4}
			\multicolumn{1}{l|}{\parbox[c][1cm]{3cm}{}}	& {\parbox[c][1cm]{4cm}{Name}} &{\parbox[c][1cm]{3cm} {Date} }&  \parbox[c][1cm]{5cm}{Signature} \\
			\cline{1-4}
			\multicolumn{1}{|c|}{\parbox [2cm]{3cm}{\centering Composed}} &{\parbox[c][2cm]{4cm}{ \@reportauthor}} &  & \\
			\cline{1-4}

		\end{tabular} \par\vskip-1.4pt

		\ifx\@reportreviewFirst\empty
		\else
		\begin{tabular}{|p{3cm}|p{4cm}|p{3cm}|p{5cm}|}
			\multicolumn{1}{|c|}{\parbox [2cm]{3cm}{\centering Approved}}&{\parbox[c][2cm]{4cm}{ \@reportreviewFirst}}  & & \\
			\cline{1-4}
		\end{tabular}\par\vskip-1.4pt
		\fi

		\ifx\@reportreviewSecond\empty
		\else
		\begin{tabular}{|p{3cm}|p{4cm}|p{3cm}|p{5cm}|}
			\multicolumn{1}{|c|}{\parbox [2cm]{3cm}{\centering Approved}}&{\parbox[c][2cm]{4cm}{ \@reportreviewSecond}}  & & \\
			\cline{1-4}
		\end{tabular}\par\vskip-1.4pt
		\fi

		\ifx\@reportreviewThird\empty
		\else
		\begin{tabular}{|p{3cm}|p{4cm}|p{3cm}|p{5cm}|}
			\multicolumn{1}{|c|}{\parbox [2cm]{3cm}{\centering Approved}}&{\parbox[c][2cm]{4cm}{ \@reportreviewThird}}  & & \\
			\cline{1-4}
		\end{tabular}\par\vskip-1.4pt
		\fi

		\ifx\@reportapprove\empty
		\else
		\begin{tabular}{|p{3cm}|p{4cm}|p{3cm}|p{5cm}|}
			\multicolumn{1}{|c|}{\parbox [2cm]{3cm}{\centering Approved}}&{\parbox[c][2cm]{4cm}{ \@reportapprove}}  & & \\
			\cline{1-4}
		\end{tabular}\par\vskip-1.4pt
		\fi
	\end{table}

	\noindent \textbf{{\large Disclaimer}} \\[0.5cm]
	\noindent This document has been prepared by \MTIname (MTI) for \@reportclient.
	The opinions and information in this report are entirely those of MTI, based on data and assumptions as reported throughout the text and upon information and data obtained from sources which MTI believes to be reliable.\\

	\noindent While MTI has taken all reasonable care to ensure that the facts and opinions expressed in this document are accurate, it makes no guarantee to any person, organization or company, representation or warranty, express or implied as to fairness, accuracy and liability for any loss howsoever, arising directly or indirectly from its use or contents.\\

	\noindent This document is intended for use by professionals or institutions. This document remains the property of MTI. All rights reserved. This document or any part thereof may not be made public or disclosed, copied or otherwise reproduced or used in any form or by any means, without prior permission in writing from MTI. \\

	\noindent This document is dated \@reportdate \\}

% ===COVERPAGE==================================================================
\newcommand{\makecover}
{
	\incgraph[overlay={
		\node at ([yshift=6.8cm] page.center) {	\includegraphics[height=3.2cm]{resources/MTIlogo.png}};
		\node at ([yshift=5cm] page.center) {\Huge \textbf{\MTIname}};
		\node at ([yshift=-1.8cm]page.center) {\large \textbf{\@reportnumber}};
		\node [align=center] at ([yshift=-2.6cm]page.center) {\large \textbf{\@reporttitle} \\ \normalsize \@reportsubtitle};
		\node[anchor=west] at ([xshift=2cm, yshift=4cm] page.south west) {\normalsize \textbf{\MTIname}};
		\node[anchor=west] at ([xshift=2cm, yshift=3.2cm] page.south west) {\normalsize \MTIBox};
		\node[anchor=west] at ([xshift=2cm, yshift=2.8cm] page.south west) {\normalsize \MTIadress};
		\node[anchor=west] at ([xshift=2cm, yshift=2cm] page.south west) {\normalsize \MTItel};
		\node[anchor=west] at ([xshift=2cm, yshift=1.6cm] page.south west) {\normalsize \MTImail};
		\node[anchor=west] at ([xshift=2cm, yshift=1.2cm] page.south west) {\normalsize \MTIsite};
		\node[anchor=west] [gray] at ([xshift=15cm, yshift=4.8cm] page.south west) {\Large \textbf{\textit{RESEARCH AND }}};
		\node[anchor=west] [gray] at ([xshift=15cm, yshift=4.2cm] page.south west) {\Large \textbf{\textit{DEVELOPMENT}}};
		\node[anchor=west] [gray] at ([xshift=15cm, yshift=3cm] page.south west) {\Large \textbf{\textit{CONSULTANCY}}};
		\node[anchor=west] [gray] at ([xshift=15cm, yshift=1.8cm] page.south west) {\Large \textbf{\textit{MEASURING AND}}};
		\node[anchor=west] [gray] at ([xshift=15cm, yshift=1.2cm] page.south west) {\Large \textbf{\textit{DIAGNOSTICS}}};}]
	[width=\paperwidth,height=\paperheight]{resources/background.png}
}

% ===BLANKPAGE==================================================================
\newcommand\blankpage{%
	\null
	\thispagestyle{empty}%
	\addtocounter{page}{-1}%
	\newpage}

% ==TITLEPAGE===================================================================
\newcommand{\maketitlepage}{
	\begin{titlepage}
		\vspace*{2cm}
		\centering
		\noindent
		\textbf{\@reporttitle}
		\\
		\@reportsubtitle
		\vspace*{1.5cm}
		\begin{table}[H]
			\centering
			\begin{tabular}{p{3 cm} p{3 cm} p{3 cm} p{3 cm} p{3 cm}}
				\footnotesize{Date}&\footnotesize{Report number} & \footnotesize{Version} & \footnotesize{Status} & \footnotesize{Author}\\
				\footnotesize{\@reportdate} & \footnotesize{\@reportnumber}  & \footnotesize{\@reportversion} & \footnotesize{\@reportstatus} &\footnotesize{\@reportauthor}
			\end{tabular}
		\end{table}

		\begin{table}[b]
			\begin{tabular}{p{4 cm} p{3 cm}}
				\textbf{Client} & \@reportclient \\\\
				\textbf{Reference number} & \@reportRefNum \\\\
				\textbf{Classification} & \@reportclassification \\\\
				\textbf{MTI order number} & \@ordernum
			\end{tabular}
		\end{table}
	\end{titlepage}
}

% ===BIBLIOGRAPHY===============================================================
\usepackage[
backend=bibtex,
style=ieee,
sorting=ynt
]{biblatex}

% ====UNITS=====================================================================
\usepackage[per-mode=fraction,
			separate-uncertainty=true,
			bracket-unit-denominator=false,
			multi-part-units=single]{siunitx}
\newcommand{\glsmath}[1]{\ensuremath{\gls{#1}\mathbin{\color{mti_gray}\left[\glsunit{#1}\right]}}}

\DeclareSIUnit{\clothing}{clo}
\DeclareSIUnit{\metabolicrate}{met}
\DeclareSIUnit{\person}{pers}

% ===GLOSSARY===================================================================
\usepackage[acronym,toc]{glossaries}

\glsaddkey{unit}{\glsentrytext{\glslabel}}{\glsentryunit}{\GLsentryunit}{\glsunit}{\Glsunit}{\GLSunit}
\newglossary[slg]{symbolslist}{sys}{syo}{Symbolslist}

\setlength{\glsdescwidth}{11cm}
\setlength{\LTleft}{0pt}
\newglossarystyle{symbunitlong}{
	\setglossarystyle{long3col}
	\renewenvironment{theglossary}{
		\begin{longtable}{lp{\glsdescwidth}>{\centering\arraybackslash}p{2cm}}}
		{\end{longtable}}
	\renewcommand*{\glossaryheader}{
		\bfseries Sign & \bfseries Description & \bfseries Unit \\
		\hline
		\endhead}
	\renewcommand*{\glossentry}[2]{
		\glstarget{##1}{\glossentryname{##1}} & \glossentrydesc{##1} & \glsunit{##1}  \tabularnewline
	}
}

\newglossarystyle{mtilong}{
	\setglossarystyle{long3col}
	\renewenvironment{theglossary}{
		\begin{longtable}{lp{\glsdescwidth}>{\arraybackslash}p{2cm}}}
		{\end{longtable}}
	\renewcommand*{\glossaryheader}{
		\bfseries Key & \bfseries Description & \bfseries Page \\
		\hline
		\endhead}
	\renewcommand*{\glossentry}[2]{
		\glstarget{##1}{\glossentryname{##1}} & \glossentrydesc{##1} & ##2 \tabularnewline
	}
}

\makeglossaries

\newcommand{\printmtiglossary}{
	\clearpage
	\printglossary[type=symbolslist,style=symbunitlong]
	\vspace{20mm}
	\printglossary[type=\acronymtype,style=mtilong]
	\vspace{20mm}
	\printglossary[type=main,style=mtilong]
	\vspace{20mm}
}

% ===TIKZ=======================================================================
\usepackage{tikz}
\usepackage{pgf}
\usepackage[siunitx,rotatelabels]{circuitikz}
\usetikzlibrary{shapes,arrows,positioning,calc,circuits}
\usetikzlibrary{decorations.markings}
\usetikzlibrary{shapes.geometric}

\tikzset{
	block/.style = {draw, fill=white, rectangle, minimum height=3em, minimum width=3em, rounded corners=3mm, fill=gray!20},
	tmp/.style  = {coordinate},
	sum/.style= {draw, fill=white, circle, node distance=1cm},
	input/.style = {draw, coordinate},
	output/.style= {coordinate},
	pinstyle/.style = {pin edge={to-,thin,black}
	}
}

\pgfdeclarelayer{edgelayer}
\pgfdeclarelayer{nodelayer}
\pgfsetlayers{edgelayer,nodelayer,main}

\tikzstyle{none}=[inner sep=0pt, align=center]
\tikzstyle{dot}=[circle,draw,inner sep=1pt,fill=black]

\tikzstyle{ct_ground}=[ground]
\tikzstyle{ct_vcc}=[vcc]
\tikzstyle{ct_vee}=[vee]
\tikzstyle{ct_spdt}=[spdt]
\tikzstyle{ct_left}=[left]
\tikzstyle{ct_right}=[right]
\tikzstyle{block}=[draw, fill=white, rectangle, minimum height=3em, minimum width=3em, rounded corners=3mm, fill=gray!20, align=center]

\tikzstyle{simple}=[-,draw=Black]
\tikzstyle{ct_R}=[R]
\tikzstyle{ct_L}=[L]
\tikzstyle{ct_open}=[open]
\tikzstyle{ct_short}=[short]
\tikzstyle{arrow}=[-,draw=Black,postaction={decorate},decoration={markings,mark=at position .5 with {\arrow{>}}},line width=2.000]

% ===ENVIRONMENTS===============================================================
\usepackage[most]{tcolorbox}

\newtcolorbox[auto counter]{note}[1][]{%
  enhanced jigsaw, % better frame drawing
  borderline west={2pt}{0pt}{red}, % straight vertical line at the left edge
  sharp corners, % No rounded corners
  boxrule=0pt, % no real frame,
  fonttitle={\large\bfseries},
  coltitle={black},  % Black color for title
  title={Note \thetcbcounter:\\ },  % Fixed title
  attach title to upper, % Move the title into the box
  #1
}

\newtcolorbox[auto counter]{statement}[1][]{%
  enhanced jigsaw, % better frame drawing
  borderline west={2pt}{0pt}{red}, % straight vertical line at the left edge
  sharp corners, % No rounded corners
  boxrule=0pt, % no real frame,
  fonttitle={\large\bfseries},
  coltitle={black},  % Black color for title
  title={Statement of needs:\\ },  % Fixed title
  attach title to upper, % Move the title into the box
  #1
}
%=== MISC =====================================================================
\newcommand{\searchterm}[1]{\textcolor{mti_gray}{\textbf{#1}}}
