import numpy as np
from dill import dump, load
from datetime import datetime

from MTIpython.units import u

__all__ = ['FutureWeather', 'HistoryWeather']


# YYYYMMDD = datum (YYYY=jaar,MM=maand,DD=dag);
# HH       = tijd (HH=uur, UT.12 UT=13 MET, 14 MEZT. Uurvak 05 loopt van 04.00 UT tot 5.00 UT;
# T        = Temperatuur (in 0.1 graden Celsius) op 1.50 m hoogte tijdens de waarneming;
# TD       = Dauwpuntstemperatuur (in 0.1 graden Celsius) op 1.50 m hoogte tijdens de waarneming;
# SQ       = Duur van de zonneschijn (in 0.1 uren) per uurvak, berekend uit globale straling  (-1 for <0.05 uur);
# Q        = Globale straling (in J/cm2) per uurvak;
# P        = Luchtdruk (in 0.1 hPa) herleid naar zeeniveau, tijdens de waarneming;
# U        = Relatieve vochtigheid (in procenten) op 1.50 m hoogte tijdens de waarneming;

class HistoryWeather:
    station = 275
    r"Deelen"

    date = []

    T = []
    r"Temperatuur (in graden Celsius) op 1.50 m hoogte tijdens de waarneming"

    TD = []
    r"Dauwpuntstemperatuur (in graden Celsius) op 1.50 m hoogte tijdens de waarneming"

    SQ = []
    r"Duur van de zonneschijn (in hr)"

    Q = []
    r"Globale straling (in W/m^2)"

    P = []
    r"Luchtdruk (in Pa) herleid naar zeeniveau, tijdens de waarneming"

    U = []
    r"Relatieve vochtigheid (in procenten) op 1.50 m hoogte tijdens de waarneming"

    def __init__(self):
        pass

    def import_from_txt(self):
        date = np.genfromtxt(r'data/KNMI_20171227_hourly.txt', skip_header=18, usecols=(1), dtype='str',
                             delimiter=',')
        data = np.loadtxt(r'data/KNMI_20171227_hourly.txt', skiprows=18, usecols=(2, 3, 4, 5, 6, 7, 8),
                          delimiter=',')

        time = [str(int(h)) if h > 9 else '0' + str(int(h)) for h in data[:, 0] - 1]
        for d, t in zip(date, time):
            self.date.append(datetime.strptime(d + t, '%Y%m%d%H'))
        self.date = np.array(self.date)

        self.T = data[:, 1] * 0.1 * u.degC
        self.TD = data[:, 2] * 0.1 * u.degC
        self.SQ = data[:, 3] * 0.1 * u.hr
        self.Q = ((data[:, 4] * u.J / u.cm ** 2) / u.hr).to(u.W/u.m ** 2)
        self.P = (data[:, 5] * 0.1 * u.hPa).to(u.Pa)
        self.U = data[:, 6] / 100

    def save(self, fname):
        with open(fname, 'wb') as f:
            dump(self, f)

    @staticmethod
    def load(fname):
        with open(fname, 'rb') as f:
            weather = load(f)
        return weather


class FutureWeather:
    station = 275
    r"Deelen"

    date = []

    evmk = []
    r"Makkink crop reference evaporarion [mm] according transformed global radiation sums and daily mean temperature"

    Q = []
    r"Transformed global radiation sums [W/m2]"

    tg = []
    r"mean temperature [degrees Celsius]"

    tn = []
    r"min temperature [degrees Celsius]"

    tx = []
    r"max temperature [degrees Celsius]"

    def __init__(self):
        pass

    def import_from_txt(self):
        date = np.genfromtxt(r'data/KNMI14____2030_tg___19810101-20101231_v3.2.txt', skip_header=11, usecols=(0),
                             dtype='str')
        for d in date:
            self.date.append(datetime.strptime(d, '%Y%m%d'))
        self.date = np.array(self.date)
        self.tg = np.loadtxt(r'data/KNMI14____2030_tg___19810101-20101231_v3.2.txt', skiprows=11, usecols=(6)) * u.degC
        self.tn = np.loadtxt(r'data/KNMI14____2030_tn___19810101-20101231_v3.2.txt', skiprows=11, usecols=(6)) * u.degC
        self.tx = np.loadtxt(r'data/KNMI14____2030_tx___19810101-20101231_v3.2.txt', skiprows=11, usecols=(6)) * u.degC
        self.evmk = np.loadtxt(r'data/KNMI14____2030_evmk___19810101-20101231_v3.2.txt', skiprows=12,
                               usecols=(6)) * u.mm

        # Transformed rsds is sum of day, need to average it over the actual hours, for the referenced time period
        solarduration = np.loadtxt(r'data/KNMI_20101231.txt', skiprows=14, usecols=(2), delimiter=',') * 0.1 * u.hr
        rsds = np.loadtxt(r'data/KNMI14____2030_rsds___19810101-20101231_v3.2.txt', skiprows=11,
                          usecols=(6)) * u.kJ / u.m ** 2
        self.Q = (rsds / solarduration).to('W/m**2')
        self.Q[np.isinf(self.Q)] = 0. * u.W / u.m ** 2
        self.Q[self.Q.m < 0] = 0. * u.W / u.m ** 2

    def save(self, fname):
        with open(fname, 'wb') as f:
            dump(self, f)

    @staticmethod
    def load(fname):
        with open(fname, 'rb') as f:
            weather = load(f)
        return weather
