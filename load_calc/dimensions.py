from MTIpython.units.SI import u

__all__ = ['outside', 'inside', 'surface', 'A_w', 'V']

surface = [403.76] * u.m ** 2

H = {'total': 6. * u.m,
     'window': 3. * u.m,
     'door': 2.2 * u.m}

V = surface[0] * H['total']

n_in = 8
inside = [dict() for d in range(n_in)]
total_width_in = [None for w in range(n_in)]
door_width = [None for d in range(n_in)]

n_out = 6
outside = [dict() for d in range(n_out)]
total_width_out = [None for w in range(n_out)]
window_width = [None for d in range(n_out)]

# wall outside 0
total_width_out[0] = 25. * u.m
window_width[0] = 18. * u.m
outside[0]['window'] = window_width[0] * H['window']
outside[0]['stone'] = total_width_out[0] * H['total'] - outside[0]['window']

# wall outside 1
total_width_out[1] = 8.9 * u.m
outside[1]['stone'] = total_width_out[1] * H['total']

# wall outside 2
total_width_out[2] = 16.1 * u.m
window_width[2] = 4. * u.m
outside[2]['stone'] = total_width_out[2] * H['total']
outside[2]['window'] = window_width[2] * H['window']

# wall outside 3
total_width_out[3] = 4.3 * u.m
outside[3]['stone'] = total_width_out[3] * H['total']

# wall outside 4
total_width_out[4] = 0.5 * u.m
outside[4]['stone'] = total_width_out[4] * H['total']

# wall outside 5
outside[5]['stone'] = outside[4]['stone']

# wall inside 0
total_width_in[0] = 10. * u.m
door_width[0] = 1.6 * u.m
inside[0]['door'] = door_width[0] * H['door']
inside[0]['stone'] = total_width_in[0] * H['total'] - inside[0]['door']

# wall inside 1
inside[1]['stone'] = outside[4]['stone']

# wall inside 2
inside[2]['stone'] = outside[4]['stone']

# wall inside 3
total_width_in[3] = 8.9 * u.m
inside[3]['stone'] = total_width_in[3] * H['total']

# wall inside 4
inside[4]['stone'] = inside[3]['stone']

# wall inside 5
total_width_in[5] = 12.5 * u.m
inside[5]['stone'] = total_width_in[5] * H['total']

# wall inside 6
total_width_in[6] = 7.9 * u.m
door_width[6] = [1.6, 1.6] * u.m
inside[6]['door'] = sum(door_width[6]) * H['door']
inside[6]['stone'] = total_width_in[6] * H['total'] - inside[6]['door']

# wall inside 7
total_width_in[7] = 8.2 * u.m
inside[7]['stone'] = total_width_in[7] * H['total']

A_w = sum([w['stone'] for w in inside]) + 2 * surface[0]
