from MTIpython.units.SI import u
from MTIpython.material.solid import Solid
from MTIpython.material.gas import MoistAir
from MTIpython.material.core import specific_volume
from MTIpython.core.validate import Check
from MTIpython.mtimath.types import inf

__all__ = ['air_outside', 'air_aula', 'brick_outside', 'brick_inside', 'glass', 'wood', 'concrete', 'steel', 'asphalt',
           'glasswool', 'firetcloth']

air_outside = MoistAir(name='Air Outside', T=22.2 * u.degC, RH=0.52, P=1.026 * u.bar)
air_outside.m = 1. * u.kg

air_aula = MoistAir(name='Air Aula', T=22.2 * u.degC, RH=0.52, P=1.026 * u.bar)
air_aula.m = 1. * u.kg

brick_outside = Solid(name='Building Brick Face', T=20. * u.degC, rho=2000. * u.kg / u.m ** 3,
                      c_p=0.84 * u.kJ / (u.kg * u.K), k=1.32 * u.W / (u.m * u.K),
                      alpha_therm_diff=5.2e-7 * u.m ** 2 / u.s, alpha_rad=0.55, epsilon_rad=0.93)

brick_outside.v = Check(value=specific_volume, bounds=(0., inf), unit=u.m ** 3 / u.kg, rho='rho')

brick_inside = Solid(name='Building Brick', T=20. * u.degC, rho=1600. * u.kg / u.m ** 3,
                     c_p=0.84 * u.kJ / (u.kg * u.K), k=1.32 * u.W / (u.m * u.K),
                     alpha_therm_diff=5.2e-7 * u.m ** 2 / u.s, alpha_rad=0.55)
brick_inside.v = Check(value=specific_volume, bounds=(0., inf), unit=u.m ** 3 / u.kg, rho='rho')

glass = Solid(name='Glass', T=20. * u.degC, rho=2700 * u.kg / u.m ** 3, c_p=0.84 * u.kJ / (u.kg * u.K),
              k=0.78 * u.W / (u.m * u.K), alpha_therm_diff=3.4e-7 * u.m ** 2 / u.s, epsilon_rad=0.94)

wood = Solid(name='Wood Fir', T=23. * u.degC, rho=420. * u.kg / u.m ** 3, c_p=2.72 * u.kJ / (u.kg * u.K),
             k=0.11 * u.W / (u.m * u.K))
wood.v = Check(value=specific_volume, bounds=(0., inf), unit=u.m ** 3 / u.kg, rho='rho')

concrete = Solid(name='Structural Concrete normal', T=30. * u.degC, rho=2260 * u.kg / u.m ** 3,
                 k=2.32 * u.W / (u.m * u.K))
concrete.v = Check(value=specific_volume, bounds=(0., inf), unit=u.m ** 3 / u.kg, rho='rho')

steel = Solid(name='Carbon steel S235JR', T=20. * u.degC, rho=7833 * u.kg / u.m ** 3, c_p=0.465 * u.kJ / (u.kg * u.K),
              alpha_therm_diff=1.474e-5 * u.m ** 2 / u.s, epsilon_rad=0.2, k=54 * u.W / (u.m * u.K))

steel.v = Check(value=specific_volume, bounds=(0., inf), unit=u.m ** 3 / u.kg, rho='rho')

asphalt = Solid(name='Asphalt', T=20. * u.degC, rho=1200 * u.kg / u.m ** 3, k=0.75, alpha_rad=0.82)
asphalt.v = Check(value=specific_volume, bounds=(0., inf), unit=u.m ** 3 / u.kg, rho='rho')

firetcloth = Solid(name='Firet Cloth', T=20. * u.degC, rho=300. * u.kg / u.m ** 3, k=0.23)
firetcloth.v = Check(value=specific_volume, bounds=(0., inf), unit=u.m ** 3 / u.kg, rho='rho')

glasswool = Solid(name='Glass Wool', T=23. * u.degC, rho=24. * u.kg / u.m ** 3,
                  c_p=0.7 * u.kJ / (u.kg * u.K), k=0.038 * u.W / (u.m * u.K), alpha_therm_diff=22.6e-7 * u.m ** 2 / u.s)
glasswool.v = Check(value=specific_volume, bounds=(0., inf), unit=u.m ** 3 / u.kg, rho='rho')
