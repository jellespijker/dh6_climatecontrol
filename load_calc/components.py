from materials import brick_inside, brick_outside, glasswool, glass, asphalt, wood, concrete, firetcloth, air_outside, \
    steel

from MTIpython.thermo.network import HeatSourceNode, HeatFlowNode, Edge, Convection, Conduction, \
    HeatTransferCoefConvection
from MTIpython.units.SI import u
from MTIpython.thermo.network import Model
from MTIpython.mtimath.types import pi

__all__ = ['build_door', 'build_inside_wall', 'build_outside_wall', 'build_outside_window', 'build_floor', 'build_roof']


def build_outside_wall(name, A, G, sink):
    nodes = [HeatSourceNode(name='source_outside_wall_{}'.format(name))]
    nodes.extend([HeatFlowNode(name='n{}_walloutside_{}'.format(name, i)) for i in range(6)])

    wall = [
        Edge(),
        Convection(h=HeatTransferCoefConvection.BUILDING_COMP_STILL_AIR_HORIZONTAL, A=A),
        Conduction(d=65. * u.mm, material=brick_outside, A=A),
        Convection(h=HeatTransferCoefConvection.ACROSS_25_MM_AIR_GAP, A=A),
        Conduction(d=10. * u.mm, material=glasswool, A=A),
        Conduction(d=40. * u.mm, material=brick_inside, A=A),
        Convection(h=HeatTransferCoefConvection.FREE_DT30_VERT_PLATE, A=A),
        Conduction(d=50. * u.mm, material=steel, A=pi / 4 * 3.6 * 4)
    ]

    Model.add_nodes(G, nodes)
    Model.add_edge(G, 'source_outside_wall_{}'.format(name), 'n{}_walloutside_0'.format(name), wall[0])
    Model.add_edge(G, 'n{}_walloutside_0'.format(name), 'n{}_walloutside_1'.format(name), wall[1])
    Model.add_edge(G, 'n{}_walloutside_1'.format(name), 'n{}_walloutside_2'.format(name), wall[2])
    Model.add_edge(G, 'n{}_walloutside_2'.format(name), 'n{}_walloutside_3'.format(name), wall[3])
    Model.add_edge(G, 'n{}_walloutside_3'.format(name), 'n{}_walloutside_4'.format(name), wall[4])
    Model.add_edge(G, 'n{}_walloutside_4'.format(name), 'n{}_walloutside_5'.format(name), wall[5])
    # Model.add_edge(G, 'n{}_walloutside_5'.format(name), 'n{}_walloutside_6'.format(name), wall[6])
    Model.add_edge(G, 'n{}_walloutside_5'.format(name), sink, wall[6])
    Model.add_edge(G, 'n{}_walloutside_2'.format(name), 'n{}_walloutside_5'.format(name), wall[7])


def build_inside_wall(name, A, G, sink, source=None):
    if source is None:
        nodes = [HeatSourceNode(name='source_wallinside_{}'.format(name))]
    else:
        nodes = [G.nodes[source]['node']]
    nodes.extend([HeatFlowNode(name='n{}_wallinside_{}'.format(name, i)) for i in range(3)])

    wall = [
        Edge(),
        Convection(h=HeatTransferCoefConvection.FREE_DT30_VERT_PLATE, A=A),
        Conduction(d=50. * u.mm, material=brick_inside, A=A),
        Convection(h=HeatTransferCoefConvection.FREE_DT30_VERT_PLATE, A=A)
    ]

    Model.add_nodes(G, nodes)
    if source is None:
        Model.add_edge(G, 'source_wallinside_{}'.format(name), 'n{}_wallinside_0'.format(name), wall[0])
    else:
        Model.add_edge(G, source, 'n{}_wallinside_0'.format(name), wall[0])
    Model.add_edge(G, 'n{}_wallinside_0'.format(name), 'n{}_wallinside_1'.format(name), wall[1])
    Model.add_edge(G, 'n{}_wallinside_1'.format(name), 'n{}_wallinside_2'.format(name), wall[2])
    #Model.add_edge(G, 'n{}_wallinside_2'.format(name), 'n{}_wallinside_3'.format(name), wall[3])
    Model.add_edge(G, 'n{}_wallinside_2'.format(name), sink, wall[3])


def build_door(name, A, G, sink):
    nodes = [HeatSourceNode(name='source_door_{}'.format(name))]
    nodes.extend([HeatFlowNode(name='n{}_door_{}'.format(name, i)) for i in range(3)])

    door = [
        Edge(),
        Convection(h=HeatTransferCoefConvection.FREE_DT30_VERT_PLATE, A=A),
        Conduction(d=10. * u.mm, material=glass, A=A),
        Convection(h=HeatTransferCoefConvection.FREE_DT30_VERT_PLATE, A=A)
    ]

    Model.add_nodes(G, nodes)
    Model.add_edge(G, 'source_door_{}'.format(name), 'n{}_door_1'.format(name), door[0])
    Model.add_edge(G, 'n{}_door_0'.format(name), 'n{}_door_1'.format(name), door[1])
    Model.add_edge(G, 'n{}_door_1'.format(name), 'n{}_door_2'.format(name), door[2])
    # Model.add_edge(G, 'n{}_door_2'.format(name), 'n{}_door_3'.format(name), door[3])
    Model.add_edge(G, 'n{}_door_2'.format(name), sink, door[3])


def build_outside_window(name, A, G, sink):
    nodes = [
        HeatSourceNode(name='source_window_{}'.format(name)),
        HeatSourceNode(name='n{}_window_sun'.format(name))
    ]
    nodes.extend([HeatFlowNode(name='n{}_window_{}'.format(name, i)) for i in range(5)])

    window = [
        Edge(),
        Convection(h=HeatTransferCoefConvection.BUILDING_COMP_STILL_AIR_HORIZONTAL, A=A),
        Conduction(d=10. * u.mm, material=glass, A=A),
        Convection(h=HeatTransferCoefConvection.ACROSS_25_MM_AIR_GAP, A=A),
        Conduction(d=10. * u.mm, material=glass, A=A),
        Convection(h=HeatTransferCoefConvection.FREE_DT30_VERT_PLATE, A=A),
        Edge()
    ]

    Model.add_nodes(G, nodes)
    Model.add_edge(G, 'source_window_{}'.format(name), 'n{}_window_0'.format(name), window[0])
    Model.add_edge(G, 'n{}_window_0'.format(name), 'n{}_window_1'.format(name), window[1])
    Model.add_edge(G, 'n{}_window_1'.format(name), 'n{}_window_2'.format(name), window[2])
    Model.add_edge(G, 'n{}_window_2'.format(name), 'n{}_window_3'.format(name), window[3])
    Model.add_edge(G, 'n{}_window_3'.format(name), 'n{}_window_4'.format(name), window[4])
    # Model.add_edge(G, 'n{}_window_4'.format(name), 'n{}_window_5'.format(name), window[5])
    Model.add_edge(G, 'n{}_window_4'.format(name), sink, window[5])
    Model.add_edge(G, 'n{}_window_sun'.format(name), sink, window[6])


def build_roof(A, G, sink):
    name = 'roof'
    nodes = [HeatSourceNode(name='source_{}'.format(name))]
    nodes.extend([HeatFlowNode(name='{}_{}'.format(name, i)) for i in range(11)])

    roof = [
        Edge(),
        Convection(h=HeatTransferCoefConvection.BUILDING_COMP_STILL_AIR_HORIZONTAL, A=A),
        Conduction(d=20. * u.mm, material=asphalt, A=A),
        Conduction(d=80. * u.mm, material=concrete, A=A),
        Convection(h=HeatTransferCoefConvection.BUILDING_COMP_STILL_AIR_VERTICAL, A=A),
        Conduction(d=50. * u.mm, material=air_outside, A=A),
        Convection(h=HeatTransferCoefConvection.BUILDING_COMP_STILL_AIR_VERTICAL, A=A),
        Conduction(d=50. * u.mm, material=wood, A=A),
        Conduction(d=60. * u.mm, material=glasswool, A=A),
        Conduction(d=10. * u.mm, material=firetcloth, A=A),
        Conduction(d=28. * u.mm, material=wood, A=A),
        Convection(h=HeatTransferCoefConvection.BUILDING_COMP_STILL_AIR_HORIZONTAL, A=A)
    ]

    Model.add_nodes(G, nodes)
    Model.add_edge(G, 'source_{}'.format(name), '{}_0'.format(name), roof[0])
    Model.add_edge(G, '{}_0'.format(name), '{}_1'.format(name), roof[1])
    Model.add_edge(G, '{}_1'.format(name), '{}_2'.format(name), roof[2])
    Model.add_edge(G, '{}_2'.format(name), '{}_3'.format(name), roof[3])
    Model.add_edge(G, '{}_3'.format(name), '{}_4'.format(name), roof[4])
    Model.add_edge(G, '{}_4'.format(name), '{}_5'.format(name), roof[5])
    Model.add_edge(G, '{}_5'.format(name), '{}_6'.format(name), roof[6])
    Model.add_edge(G, '{}_6'.format(name), '{}_7'.format(name), roof[7])
    Model.add_edge(G, '{}_7'.format(name), '{}_8'.format(name), roof[8])
    Model.add_edge(G, '{}_8'.format(name), '{}_9'.format(name), roof[9])
    Model.add_edge(G, '{}_9'.format(name), '{}_10'.format(name), roof[10])
    # Model.add_edge(G, '{}_10'.format(name), '{}_11'.format(name), roof[11])
    Model.add_edge(G, '{}_10'.format(name), sink, roof[11])


def build_floor(A, G, sink):
    name = 'floor'
    nodes = [HeatSourceNode(name='source_{}'.format(name))]
    nodes.extend([HeatFlowNode(name='{}_{}'.format(name, i)) for i in range(6)])

    floor = [
        Edge(),
        Conduction(d=80. * u.cm, material=air_outside, A=A),
        Convection(h=HeatTransferCoefConvection.BUILDING_COMP_STILL_AIR_HORIZONTAL, A=A),
        Conduction(d=80. * u.mm, material=concrete, A=A),
        Conduction(d=50. * u.mm, material=glasswool, A=A),
        Conduction(d=50. * u.mm, material=concrete, A=A),
        Convection(h=HeatTransferCoefConvection.BUILDING_COMP_STILL_AIR_HORIZONTAL, A=A)
    ]

    Model.add_nodes(G, nodes)
    Model.add_edge(G, 'source_{}'.format(name), '{}_0'.format(name), floor[0])
    Model.add_edge(G, '{}_0'.format(name), '{}_1'.format(name), floor[1])
    Model.add_edge(G, '{}_1'.format(name), '{}_2'.format(name), floor[2])
    Model.add_edge(G, '{}_2'.format(name), '{}_3'.format(name), floor[3])
    Model.add_edge(G, '{}_3'.format(name), '{}_4'.format(name), floor[4])
    Model.add_edge(G, '{}_4'.format(name), '{}_5'.format(name), floor[5])
    # Model.add_edge(G, '{}_5'.format(name), '{}_6'.format(name), floor[6])
    Model.add_edge(G, '{}_5'.format(name), sink, floor[5])
