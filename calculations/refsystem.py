# %% Refrigerant system dimension
import MTIpython as mti
from MTIpython.fluid.principal.core import *
from MTIpython.fluid.principal.design import *
from scipy.optimize import minimize
import numpy as np
from math import pi
u = mti.u

# %% Materials
nh3: mti.material.Gas = mti.material.material_factory['ammonia']
nh3.T = -20 * u.degC

# %% System designs
v_max = 30 * u.m / u.s
p_max = 20. * u.bar
m_flow = 0.4 * u.kg / u.s
Q = m_flow / nh3.rho
# d_prime = 50. * u.mm
L = 10. * u.m
epsilon = 2.4e-4 * u.m


# %% Solve design diameter
def design_diameter(args):
    d_prime = args[0] * u.mm
    v = Q / (pi/4 * d_prime**2)

    re_nr = Re(fluid=nh3, L=d_prime, v=v, category=mti.material.base.Category.FLUID)
    f = friction_factor(Re=re_nr, epsilon=epsilon, d=d_prime, eqType='Haaland')
    h_l = darcy(f=f, l=L, d=d_prime, v=v, retType='headloss')
    d = pipe_diameter(epsilon=epsilon, L=L, h_l=h_l, fluid=nh3, Q=Q)
    print(d.to('mm'))
    print(v.to('m/s'))
    return abs((d - d_prime).to('mm').m)


# solve
x0 = [150., ]
bnds = ((20, 700.),)
res = minimize(design_diameter, x0, method='SLSQP', bounds=bnds)
print("d_prime: design_diameter({}) -> {}".format(res.x, design_diameter(res.x)))
A = Q/v_max
d_flow = (A/(pi/4))**(1/2)
print("Allowed diamater according to flow {}".format(d_flow))
