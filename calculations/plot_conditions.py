# -*- coding: utf-8 -*-
from copy import deepcopy

import matplotlib.pyplot as plt
from matplotlib import rcParams
from matplotlib.font_manager import FontProperties
import numpy as np
from MTIpython import u
from MTIpython.material import material_factory

# %% Known variables
coolingloads = [50., -15.] * u.kW
air_entree = material_factory['air_humid']
persons = 100. * 5
airflow_per_person = 43.3 * u.m ** 3 / u.hr
airflow_total = airflow_per_person * persons

# Conditions aula
temperatures = [20., 22] * u.degC
# for temperature, coolingload in zip(temperatures, coolingloads):
air_aula = material_factory['air_humid']
air_aula.V = 403.76 * u.m ** 2 * 6. * u.m
air_aula.T = temperatures[1]
air_aula.RH = 0.5


# Function to optimize
def f(params):
    # Set inlet air conditions
    air_e = deepcopy(air_entree)
    air_e.T = params[0] * u.degC
    air_e.RH = params[1]
    air_e.V = airflow_total * u.s
    # Update aula air conditions
    air_a = deepcopy(air_aula)
    air_a.m = air_e.m
    res = (air_a.h - air_e.h).to('J/kg').m * air_e.m.to('kg').m
    return abs(coolingloads[1].to('W').m - res)

# %% Calculate results
T = np.arange(15., 29., 0.25)
RH = np.arange(0.2, 0.6, 0.01)
tv, rhv = np.meshgrid(T, RH, sparse=False)
dq = np.zeros((len(T), len(RH)))
for i in range(len(T)):
    for j in range(len(RH)):
        dq[i, j] = f([tv[j, i], rhv[j, i]])

# %% Plot
font = FontProperties()
font.set_file('Raleway-Black.ttf')
font.set_size('x-large')
font_ax = FontProperties()
font_ax.set_file('Lato-Regular.ttf')

fig, ax = plt.subplots()
plt.title("TEMPERATURE & REL. HUMIDITY", fontproperties=font)
plt.xlabel("Temperature in [C°]", fontproperties=font_ax)
plt.ylabel("Relative Humidity in [%]", fontproperties=font_ax)
p = ax.pcolor(T, RH, (dq.T * u.W).to('kW').m, cmap=plt.cm.plasma.reversed(), vmin=np.min((dq* u.W).to('kW').m), vmax=np.max((dq* u.W).to('kW').m))
cb = fig.colorbar(p)
cb.set_label("dq in [kW]", fontproperties=font_ax)
plt.tight_layout()
