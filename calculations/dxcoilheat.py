from math import log, pi
from typing import List

import MTIpython as mti
import numpy as np

u = mti.u

s_height = 40. * u.mm
s_depth = 40. * u.mm
height = 440. * u.mm
p_n = 9
p_m = 10
p_r = 9
n_pipe = p_n * 5 + p_m * 4

d = 20. * u.mm
v = 4. * u.m / u.s

air: List[mti.material.HumidAir] = [mti.material.material_factory['air_humid'],
                                    mti.material.material_factory['air_humid'],
                                    mti.material.material_factory['air_humid'],
                                    mti.material.material_factory['air_humid']]
for a in air:
    a.W = 0. * u.g / u.kg

# Air at inlet
air[0].V = 26.22 * u.m ** 3
air[0].T = 10.03 * u.degC
mti.core.binder.lock(air[0].T)
air[0].RH = .6
mti.core.binder.lock(air[0].RH)
mti.core.binder.lock(air[0].W)
mti.core.binder.lock(air[0].m)
mti.core.binder.unlock(air[0].T)
mti.core.binder.unlock(air[0].RH)

# Air at exit
air[2].W = air[0].W
mti.core.binder.lock(air[2].W)
air[2].T = 20. * u.degC
air[2].m = air[0].m
mti.core.binder.lock(air[2].m)

# Air between
air[1].T = np.mean([air[0].T.m, air[2].T.m]) * u.degC
air[1].W = air[0].W
mti.core.binder.lock(air[2].W)
air[1].m = air[0].m
mti.core.binder.lock(air[2].m)

# Air film
air[3].W = air[0].W
mti.core.binder.lock(air[2].W)
air[3].m = air[0].m
mti.core.binder.lock(air[2].m)

# NH3 materials
nh3: List[mti.material.Gas] = [mti.material.material_factory['ammonia'],
                               mti.material.material_factory['ammonia'],
                               mti.material.material_factory['ammonia']]
nh3[0].T = -20. * u.degC
nh3[1].T = np.mean([nh3[0].T.m, nh3[2].T.m]) * u.degC

# film temperature
air[3].T = ((nh3[1].T.to('K') + air[1].T.to('K')) / 2)

# Determine conditions
cond = ((s_height / 2) ** 2 + s_depth ** 2) ** (1 / 2)
if cond < ((s_height + d) / 2):
    h_2 = ((s_height / 2) ** 2 + s_depth ** 2) ** (1 / 2) - d
    v_max = v * (s_height / 2) / h_2
else:
    v_max = v * s_height / (s_height - d)
Re_max = mti.fluid.core.Re(fluid=air[1], v=v_max, L=d)
Re_max.ito(u.dimensionless)

C_1 = 0.4
n = 0.6
C_2 = 0.95

Nu = C_1 * C_2 * Re_max ** n * air[3].Pr ** 0.36 * (air[1].Pr / air[3].Pr) ** (1 / 4)
Nu.ito(u.dimensionless)
alpha_c = Nu * air[1].k / d
alpha_c.ito('W/(m**2 * K)')

A = 0.44 * u.m ** 2  # why
Phi_m_l = air[1].rho * A * v * u.m ** -1  # Per meter
Delta_T_air = air[2].T.to('K') - air[0].T.to('K')
Phi_w_l = Phi_m_l * air[1].c_p * (Delta_T_air)
Phi_w_l.ito('kW/m')
print(Phi_w_l)
