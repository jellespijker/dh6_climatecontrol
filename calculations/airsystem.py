# %% Refrigerant system dimension
import MTIpython as mti
from MTIpython.fluid.principal.core import *
from MTIpython.fluid.principal.design import *
from scipy.optimize import minimize

from math import pi
u = mti.u

# %% Materials
air: mti.material.HumidAir = mti.material.material_factory['air_humid']
air.T = 15. * u.degC

# %% System designs
v_max = 20 * u.m / u.s
p_max = 1 * u.bar
Q = 4.8 * u.m**3 / u.s
m_flow = Q * air.rho
L = 100. * u.m
epsilon = 2.4e-4 * u.m


# %% Solve design diameter
def design_diameter(args):
    d_prime = args[0] * u.mm
    v = Q / (pi/4 * d_prime**2)

    re_nr = Re(fluid=air, L=d_prime, v=v, category=mti.material.base.Category.FLUID)
    f = friction_factor(Re=re_nr, epsilon=epsilon, d=d_prime, eqType='Haaland')
    h_l = darcy(f=f, l=L, d=d_prime, v=v, retType='headloss')
    d = pipe_diameter(epsilon=epsilon, L=L, h_l=h_l, fluid=air, Q=Q)
    print(d.to('mm'))
    print(v.to('m/s'))
    return abs(1. - (1. + abs((d - d_prime).to('mm').m)) * (1. + abs((v-v_max).to('m/s').m)))


# solve
x0 = [150., ]
bnds = ((20, 700.),)
res = minimize(design_diameter, x0, method='SLSQP', bounds=bnds)
print("d_prime: design_diameter({}) -> {}".format(res.x, design_diameter(res.x)))
A = Q/v_max
d_flow = (A/(pi/4))**(1/2)
print("Allowed diameter according to flow {}".format(d_flow))
d = max(d_flow.to('m').m, res.x[0]) * u.m
print("Allowed chosen diameter {}".format(d))
